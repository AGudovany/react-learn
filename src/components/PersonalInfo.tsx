import {useState} from "react";
import styled from "@emotion/styled";
import {css} from "@emotion/css";

const Input = styled.input(
    {
        display: 'block',
        width: '100%',
        lineHeight: '45px',
        margin: 0,
        padding: '0 15px',
        borderRadius: '8px',
        color: 'var(--my-c-primary-1)',
        border: 'solid 1px var(--my-c-gray-4)',
        boxSizing: 'border-box',
        fontSize: '16px',
        fontFamily: 'var(--font-family-main)',
        fontWeight: '500',
    }
);

const Field = styled.div`
    font-size: 14px;
    margin-bottom: 23px;

    & div {
        display: flex;
        width: 100%;
        justify-content: space-between;
        align-items: baseline;
        padding-bottom: 4px;

        & label {
            margin-right: 10px;
            color: var(--my-c-primary-1);
        }
    }
`;

function PersonalInfo(props) {

    const personalInfo = props.currentForm.personalInfo;
    const [name, setName] = useState(personalInfo.name);
    const [email, setEmail] = useState(personalInfo.email);
    const [phone, setPhone] = useState(personalInfo.phone);

    const nameChangeHandler = (e) => {
        e.preventDefault();
        setName(e.target.value);
    };
    const emailChangeHandler = (e) => {
        e.preventDefault();
        setEmail(e.target.value);
    };
    const phoneChangeHandler = (e) => {
        e.preventDefault();
        setPhone(e.target.value);
    };

    const onClickBackButtonHandler = () => {
        props.setPage(props.currentPage - 1);
    };

    const onClickNextButtonHandler = (e) => {
        e.preventDefault();
        props.setPage(props.currentPage + 1);
        props.setFormChanges((prevState) => {
            return {
                ...prevState, personalInfo: {
                    name,
                    email,
                    phone,
                }
            }
        });
    };

    return (
        <form onSubmit={onClickNextButtonHandler}>
            <div>
                <h2 className={css`
                    font-weight: 700;
                    font-size: 32px;
                    color: var(--my-c-primary-1)
                `}>Personal info</h2>
                <p>Please provide your name, email address and phone number</p>
            </div>
            <div className="form">
                <Field>
                    <div>
                        <label>Name</label>
                    </div>
                    <Input onChange={nameChangeHandler} className="input" name='name' defaultValue={name}></Input>
                </Field>
                <Field>
                    <div>
                        <label>Email</label>
                    </div>
                    <Input onChange={emailChangeHandler} className="input" name='email' defaultValue={email}></Input>
                </Field>
                <Field>
                    <div>
                        <label>Phone</label>
                    </div>
                    <Input onChange={phoneChangeHandler} className="input" name='phone' defaultValue={phone}></Input>
                </Field>
            </div>
            <div className="bottom controls">
                        <span
                            style={{'visibility': props.currentPage === 1 ? 'hidden' : 'visible'}}>
                            <button
                                type="button"
                                className="button link" onClick={onClickBackButtonHandler}>Back
                            </button>
                        </span>
                <button
                    style={{'visibility': props.currentPage === 4 ? 'hidden' : 'visible'}}
                    className="button next"
                    type="submit">
                    <span>Next</span>
                </button>
            </div>
        </form>
    );
}

export default PersonalInfo;
