import {useState} from "react";

function Addons(props) {
    const [addons] = useState(props.addons);
    const onAddonClickHandler = (e: any) => {
        addons.forEach((addon, index) => {
            if (addon.id === e.target.value) {
                addon.selected = !addon.selected;
            }
        });
    }
    const onClickBackButtonHandler = () => {
        props.setPage( props.currentPage - 1);
    };
    const onClickNextButtonHandler = () => {
        props.setPage(props.currentPage + 1);
        props.setFormChanges(
            (prevState) => {
                return {...prevState, addons};
            }
        );
    };

    return (
        <div>
            <h2 className="head">Select your plan</h2>
            <p>You have the option of monthly or yearly billing.</p>
            <div className="form">
                <div className="checkbox">
                    {addons.map(
                        (addon, index) => {
                            return (
                                <div key={index}>
                                    <div className="checkbox-item">
                                        <input
                                            type="checkbox"
                                            onChange={onAddonClickHandler}
                                            id={addon.id}
                                            value={addon.id}
                                            defaultChecked={addon.selected}>
                                        </input>
                                        <label htmlFor={addon.id}>
                                        <span>
                                            <span className="title">
                                                {addon.title}
                                            </span>
                                            <span className="text">
                                                {addon.info}
                                            </span>
                                        </span>
                                            <span className="price">{addon.price}</span>
                                        </label>
                                    </div>
                                </div>
                            )
                        })}
                </div>
            </div>
            <div className="bottom controls">
                        <span
                            style={{"visibility": props.currentPage === 1 ? 'hidden' : 'visible'}}>
                            <button className="button link" onClick={onClickBackButtonHandler}>Back</button>
                        </span>
                <button
                    style={{"visibility": props.currentPage === 4 ? 'hidden' : 'visible'}}
                    className="button next" onClick={onClickNextButtonHandler}>
                    <span>Next</span>
                </button>
            </div>
        </div>
    );
}

export default Addons;
