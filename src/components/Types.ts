export type FormProps = {
    currentPage: number,
    setPage: (page: number) => void,
}

export type ContentProps = FormProps & {
    data: {
        plans: Plan[],
        addons: Addon[],
    },
    currentForm: CurrentForm,
    setFormChanges: (cb:(data: CurrentForm) => void) => CurrentForm,
}

export type Plan = {
    id: string,
    title: string,
    icon: string,
    price: number,
}

export type Addon = {
    id: string,
    title: string,
    info: string,
    price: number,
    selected: boolean,
}

export type PersonalInfo = {
    name: string,
    email: string,
    phone: string,
}

export type CurrentForm = {
    personalInfo: PersonalInfo,
    selectedPlan: string,
    currentAddons: Addon[],
}
