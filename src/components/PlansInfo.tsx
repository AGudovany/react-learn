import {useState} from "react";
import {Plan} from "./Types";
import {plans} from "../constant";
import styled from '@emotion/styled';

function PlansInfo(props) {
    const [selectedPlan, setPlan] = useState(props.currentForm.selectedPlan || 'arcade');
    const handlePlanChange = (e) => {
        setPlan(e.target.value);
    }
    const onClickBackButtonHandler = () => {
        props.setPage(props.currentPage - 1);
    };
    const onClickNextButtonHandler = () => {
        props.setPage(props.currentPage + 1);
        props.setFormChanges((prevState) => {
                return {...prevState, selectedPlan};
            }
        );
    };
    return (
        <div>
            <div>
                <h2 className="head">Select your plan</h2>
                <p>You have the option of monthly or yearly billing.</p>
            </div>
            <div className="form">
                <div className="radio-group plans-radio radio">
                    {plans.map((plan: Plan, index: number) => {
                        return (
                            <div key={index} className={'my-radio radio-item'}>
                                <input type="radio" id={plan.id} name="plans" value={plan.id} className="my-radio-input"
                                       onChange={handlePlanChange}
                                       checked={plan.id === selectedPlan}/>
                                <label className="my-radio-label" htmlFor={plan.id}>
                                    <div className={plan.id}/>
                                    <span className="text">
                                    <span className="title">{plan.title}</span>
                                    <span>{plan.price}</span>
                                </span>
                                </label>
                            </div>
                        )
                    })}
                </div>
            </div>
            <div className="bottom controls">
                        <span
                            style={{'visibility': props.currentPage === 1 ? 'hidden' : 'visible'}}>
                            <button className="button link" onClick={onClickBackButtonHandler}>Back</button>
                        </span>
                <button
                    style={{'visibility': props.currentPage === 4 ? 'hidden' : 'visible'}}
                    className="button next" onClick={onClickNextButtonHandler}>
                    <span>Next</span>
                </button>
            </div>
        </div>
    );
}

export default PlansInfo;
