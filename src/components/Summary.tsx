import {Plan, Addon} from "./Types";
import {plans} from "../constant";

function Summary(props) {
    const selectedPlan = plans.find((plan:Plan) => plan.id === props.currentForm.selectedPlan);
    const selectedAddons = props.currentForm.addons.filter((addon: Addon) => addon.selected);
    const totalPrice = selectedPlan.price + selectedAddons.reduce((total: number, addon : Addon) => total + addon.price, 0);
    const onClickBackButtonHandler = () => {
        props.setPage(props.currentPage - 1);
    };
    const onClickNextButtonHandler = () => {
        props.setPage(props.currentPage + 1);
    };

    return (
        <div>
            <h2 className="head">Finishing up</h2>
            <p>Double-check everything looks OK before confirming.</p>
            <div className="form summary-form">
                <div className="box">
                    <div className="summary">
                        <div className="row">
                            <div className="cell">
                                <span className="title">{selectedPlan.title}</span><br/>
                                <a className="link" onClick={ () => props.setPage(2)}>Change</a>
                            </div>
                            <div className="cell title">
                                <span>{selectedPlan.price}</span>
                            </div>
                        </div>
                        <div className="addons">
                            {selectedAddons.map((addon: Addon, index: number) => {
                                return (
                                <div key={index} className="row">
                                    <div className="cell">{addon.title}</div>
                                    <div className="cell">
                                        <span>{addon.price}</span>
                                    </div>
                                </div>
                                )})
                            }
                        </div>
                    </div>
                </div>
                <div className="summary">
                    <div className="row">
                        <div className="cell">Total (per month)</div>
                        <div className="cell total">
                            <span>{totalPrice}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div className="bottom controls">
                        <span
                            style={{"visibility": props.currentPage === 1 ? 'hidden' : 'visible'}}>
                            <button className="button link" onClick={onClickBackButtonHandler}>Back</button>
                        </span>
                <button
                    style={{"visibility": props.currentPage === 4 ? 'hidden' : 'visible'}}
                    className="button next" onClick={onClickNextButtonHandler}>
                    <span>Next</span>
                </button>
            </div>
        </div>
    );
}

export default Summary;
