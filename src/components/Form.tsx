import {useState} from "react";
import {FormProps, CurrentForm} from "./Types";
import PersonalInfo from "./PersonalInfo";
import PlansInfo from "./PlansInfo";
import Addons from "./Addons";
import Summary from "./Summary";
import {addons} from "../constant";

function Form(props: FormProps) {
    const defaultState: CurrentForm = {
        personalInfo: {
            name: '',
            email: '',
            phone: '',
        },
        currentAddons: addons,
        selectedPlan: ''
    }

    const [form, setForm] = useState(defaultState);

    const setFormChanges = (data) => {
        setForm(data)
    }

    return (
        <div className="content">
            <div className="wrapper">
                <div className="form">
                    <div className="stepper">
                        {<div className="form-step">
                            {props.currentPage === 1
                                && <PersonalInfo
                                    {...props}
                                    currentForm={form}
                                    setFormChanges = {setFormChanges}/>}
                            {props.currentPage === 2
                                && <PlansInfo
                                    {...props}
                                    currentForm={form}
                                    setFormChanges = {setFormChanges}/>}
                            {props.currentPage === 3
                                && <Addons
                                    {...props}
                                    addons = {form.currentAddons}
                                    setFormChanges = {setFormChanges}/>}
                            {props.currentPage === 4
                                && <Summary
                                    {...props}
                                    currentForm={form}/>}
                        </div>}
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Form;
