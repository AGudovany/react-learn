import {css} from "@emotion/css";
import styled from "@emotion/styled";

const Step = styled.div`
    display: flex;
    align-items: center;
    margin-bottom: 24px;
    color: var(--my-c-gray-1);
`
const Number = styled.div`
    margin: 0 16px;
    width: 32px;
    height: 32px;
    font-size: 13px;
    line-height: 32px;
    font-weight: 700;
    border: solid 1px var(--my-c-gray-1);
    border-radius: 50%;
    text-align: center;
    transition: color .3s ease, background .3s ease;
`
const NavHolder = styled.div`
    width: 275px;
    min-height: 570px;
    flex-shrink: 0;
    margin-right: 15px;
    display: flex;
    flex-direction: column;
    padding: 35px 15px;
    box-sizing: border-box;
    border-radius: 10px;
    background-color: var(--my-c-primary-2);
    background-image: url("../assets/bg-sidebar-desktop.svg");
    background-size: cover;
`

function Navigation(props) {
    const pages = [
        "Your info",
        "Plans",
        "Addons",
        "Summary"
    ];
    return (
        <NavHolder>
            {pages.map((page, index) => {
                const step = index + 1;
                return (
                    <Step key={step} >
                        <Number className={
                            props.currentPage-1 === index ?
                                css`
                                    background: var(--my-c-primary-4);
                                    color: var(--my-c-primary-2);
                                ` : css`
                                color: var(--my-c-gray-1)
                            `}>{step}</Number>
                        <div className="details">
                            <div className={css`
                                color: var(--my-c-gray-5);
                                font-size: 12px;
                                text-transform: uppercase;
                            `}>Step {step}</div>
                            <div className={css`
                                font-weight: 500;
                                font-size: 14px;
                                letter-spacing: 1px;
                                text-transform: uppercase;
                            `}>{page}</div>
                        </div>
                    </Step>
                );
            })}
        </NavHolder>
    )
}

export default Navigation;
