import './App.css';
import Navigation from './components/Navigation';
import Form from './components/Form';
import {useState} from "react";

function App() {
    const [page, setPage] = useState(1);

    const onPageChangeHandler = (page: number) => {
        setPage(page);
    };

    return (
        <div className="app">
            <Navigation setPage={onPageChangeHandler} currentPage={page}/>
            <Form setPage={onPageChangeHandler} currentPage={page}/>
        </div>
    );
}

export default App;
