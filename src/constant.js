export const plans = [
    {
        id: 'arcade',
        title: "Arcade",
        icon: "./assets/arcade.svg",
        price: 10,
    },
    {
        id: 'advanced',
        title: "Advanced",
        icon: "./assets/advanced.svg",
        price: 20,
    },
    {
        id: 'pro',
        title: "Pro",
        icon: "./assets/pro.svg",
        price: 30,
    },
];

export const addons = [
    {
        id: "services",
        title: "Online service",
        info: "Access to multiplayer games",
        price: 10,
        selected: true,
    },
    {
        id: "storage",
        title: "Large storage",
        info: "Extra 1TB of cloud save",
        price: 20,
        selected: false,
    },
    {
        id: "profile",
        title: "Customizable Profile",
        info: "Custom theme on your profile",
        price: 30,
        selected: false,
    },
];
